﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Bible {
    public class App : Application {
        public Label label_content;
        public string bible_ver;
				public static readonly List<Book> book_list = new List<Book> {
						new Book {code = "gen", max_chapter = 50},
						new Book {code = "exo", max_chapter = 40},
						new Book {code = "lev", max_chapter = 27},
						new Book {code = "num", max_chapter = 36},
						new Book {code = "deu", max_chapter = 34},
						new Book {code = "jos", max_chapter = 24},
						new Book {code = "jud", max_chapter = 21},
						new Book {code = "rut", max_chapter = 4},
						new Book {code = "1sa", max_chapter = 31},
						new Book {code = "2sa", max_chapter = 24},
						new Book {code = "1ki", max_chapter = 22},
						new Book {code = "2ki", max_chapter = 25},
						new Book {code = "1ch", max_chapter = 29},
						new Book {code = "2ch", max_chapter = 36},
						new Book {code = "ezr", max_chapter = 10},
						new Book {code = "neh", max_chapter = 13},
						new Book {code = "est", max_chapter = 10},
						new Book {code = "job", max_chapter = 42},
						new Book {code = "psa", max_chapter = 150},
						new Book {code = "pro", max_chapter = 31},
						new Book {code = "ecc", max_chapter = 12},
						new Book {code = "son", max_chapter = 8},
						new Book {code = "isa", max_chapter = 66},
						new Book {code = "jer", max_chapter = 52},
						new Book {code = "lam", max_chapter = 5},
						new Book {code = "eze", max_chapter = 48},
						new Book {code = "dan", max_chapter = 12},
						new Book {code = "hos", max_chapter = 14},
						new Book {code = "joe", max_chapter = 3},
						new Book {code = "amo", max_chapter = 9},
						new Book {code = "oba", max_chapter = 1},
						new Book {code = "jon", max_chapter = 4},
						new Book {code = "mic", max_chapter = 7},
						new Book {code = "nah", max_chapter = 3},
						new Book {code = "hab", max_chapter = 3},
						new Book {code = "zep", max_chapter = 3},
						new Book {code = "hag", max_chapter = 2},
						new Book {code = "zec", max_chapter = 14},
						new Book {code = "mal", max_chapter = 4},
						new Book {code = "mat", max_chapter = 28},
						new Book {code = "mar", max_chapter = 16},
						new Book {code = "luk", max_chapter = 24},
						new Book {code = "joh", max_chapter = 21},
						new Book {code = "act", max_chapter = 28},
						new Book {code = "rom", max_chapter = 16},
						new Book {code = "1co", max_chapter = 16},
						new Book {code = "2co", max_chapter = 13},
						new Book {code = "gal", max_chapter = 6},
						new Book {code = "eph", max_chapter = 6},
						new Book {code = "phi", max_chapter = 4},
						new Book {code = "col", max_chapter = 4},
						new Book {code = "1th", max_chapter = 5},
						new Book {code = "2th", max_chapter = 3},
						new Book {code = "1ti", max_chapter = 6},
						new Book {code = "2ti", max_chapter = 4},
						new Book {code = "tit", max_chapter = 3},
						new Book {code = "phi", max_chapter = 1},
						new Book {code = "heb", max_chapter = 13},
						new Book {code = "jam", max_chapter = 5},
						new Book {code = "1pe", max_chapter = 5},
						new Book {code = "2pe", max_chapter = 3},
						new Book {code = "1jo", max_chapter = 5},
						new Book {code = "2jo", max_chapter = 1},
						new Book {code = "3jo", max_chapter = 1},
						new Book {code = "jud", max_chapter = 1},
						new Book {code = "rev", max_chapter = 22}
				};

				public static async Task GetUrlContent(string bible_version, string book, string chapter, Action<string, string> callback_function) {
						try {
								string Url = String.Format("https://www.bible.com/bible/" + bible_version + "/" + book + "." + chapter);
								HttpClient hc = new System.Net.Http.HttpClient();
								System.Diagnostics.Debug.WriteLine("1" + " - https://www.bible.com/bible/" + bible_version + "/" + book + "." + chapter);
								string url_result = await hc.GetStringAsync(Url);
								System.Diagnostics.Debug.WriteLine("2" + " - " + url_result.Length.ToString());
								int content_start = url_result.IndexOf("<div class=\"version vid" + bible_version + " iso6393");
								if (content_start > -1) {
										System.Diagnostics.Debug.WriteLine("3" + " - " + content_start.ToString());
										int content_end = url_result.IndexOf("<div class='copyright' ng-hide='copyright'>");
										if (content_end > -1) {
												System.Diagnostics.Debug.WriteLine("4" + " - " + content_end.ToString());
												string content_result = url_result.Substring(content_start, content_end - content_start);
												System.Diagnostics.Debug.WriteLine("5");
												System.Diagnostics.Debug.WriteLine(content_result);
												callback_function(bible_version, content_result);
										}
										callback_function(bible_version, "<div>Middle to End Page</div>" + url_result);
								}
								callback_function(bible_version, "<div>Full Page</div>" + url_result);
						} catch (Exception ex) {
								callback_function(bible_version, "There is a problem getting the chapter");
						}
        }

        public App() {
						MainPage = new Home();
				}

        protected override void OnStart() {
            // Handle when your app starts
        }

        protected override void OnSleep() {
            // Handle when your app sleeps
        }

        protected override void OnResume() {
            // Handle when your app resumes
        }
    }
}
