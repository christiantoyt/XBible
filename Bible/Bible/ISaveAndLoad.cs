﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bible {
		public interface ISaveAndLoad {
				void SaveText(string filename, string text);
				string GetText(string filename);
		}
}
