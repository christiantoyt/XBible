﻿using Bible;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[assembly: Xamarin.Forms.Dependency(typeof(SaveAndLoad))]
namespace Bible {
		public class SaveAndLoad : ISaveAndLoad {
				public void SaveText(string filename, string text) {
						var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
						var filePath = Path.Combine(documentsPath, filename);
						System.IO.File.WriteAllText(filePath, text);
				}
				public string LoadText(string filename) {
						var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
						var filePath = Path.Combine(documentsPath, filename);
						return System.IO.File.ReadAllText(filePath);
				}

				public string GetText(string filename) {
						throw new NotImplementedException();
				}
		}
}
