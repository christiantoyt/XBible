﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Bible {
		public partial class Home : ContentPage {
				private Dictionary<string, WebView> bible_contents;

				void RefreshContent(string bible_version, string content) {
						button_view_chapter.IsEnabled = true;
						HtmlWebViewSource html_source = new HtmlWebViewSource();
						html_source.Html = content;
						bible_contents[bible_version].Source = html_source;
						label_status.Text = (bible_version + " loaded");
						//wv_content.Source = html_source;
				}

				void ChangeMaxChapter(object sender, EventArgs e) {
						if (picker_book.SelectedIndex > -1) {
								picker_chapter.Items.Clear();
								for (int i = 0; i < App.book_list[picker_book.SelectedIndex].max_chapter; i++) {
										picker_chapter.Items.Add((i + 1).ToString());
								}
						}
				}

				void ReloadChapter(object sender, EventArgs e) {
						if (entry_bible_ver.Text == "") {
								label_status.Text = ("Please input Bible Version");
						} else if (picker_book.SelectedIndex < 0) {
								label_status.Text = ("Please select Book");
						} else if (picker_chapter.SelectedIndex < 0) {
								label_status.Text = ("Please select Chapter");
						} else {
								string[] bible_list = entry_bible_ver.Text.Split(',');

								grid_bible_content.RowDefinitions = new RowDefinitionCollection();
								grid_bible_content.ColumnDefinitions = new ColumnDefinitionCollection();
								grid_bible_content.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
								bible_contents = new Dictionary<string, WebView>();
								for (int i = 0; i < bible_list.Length; i++) {
										bible_contents[bible_list[i]] = new WebView();

										grid_bible_content.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
										grid_bible_content.Children.Add(bible_contents[bible_list[i]], 0, i);

										App.GetUrlContent(bible_list[i], App.book_list[picker_book.SelectedIndex].code, (picker_chapter.SelectedIndex + 1).ToString(), RefreshContent);
								}

								label_status.Text = ("Please Wait");
								button_view_chapter.IsEnabled = false;
						}
				}

				public Home() {
						InitializeComponent();

						entry_bible_ver.Text = "306";

						foreach (Book i_book in App.book_list) {
								picker_book.Items.Add(i_book.code);
						}

						//label_status.Text = ("Please Wait");
						//App.GetUrlContent("306", "gen", "1", RefreshContent);
				}
		}
}
